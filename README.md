## Initial Setup

Start all the services we use with docker-compose.

```bash
docker-compose up -d
```

These applications will start:
- [gogs](https://github.com/gogs/gogs): a git project versioning tool

- [drone](https://github.com/drone/drone): a CI/CD system that is triggered with commits to git projects within gogs
- registry: a docker registry that holds build artifacts / docker images

Open the gogs UI: http://localhost:3000
 - register a new user; e.g. test:test test@test.de
 - login with the new user
 - create a repository "docker-http-server"

Open the drone UI: http://localhost:8000
 - activate CI/CD for the test project "docker-http-server"
 - trust the test repository (project settings -> Trusted)

Download a sample project

```bash
git clone https://github.com/katacoda/golang-http-server && cd golang-http-server
```

Edit .drone.yml

```yaml
---
kind: pipeline
name: build
steps:
- name: build http-server  
  image: plugins/docker
  settings:
    registry: registry:5000 
    repo: registry:5000/test/docker-http-server
    insecure: true
    tags: 
      - 1.0.0
      - 1.0
      - latest
- name: deploy http-server
  image: docker
  volumes:
  - name: dockersock
    path: /var/run/docker.sock
  commands:
  - docker kill http-server
  - sleep 2
  - docker run \
          --rm \
          --name http-server \
          --detach \
          --publish 8080:80 \
          localhost:5000/test/docker-http-server
  - docker ps | grep "http-server"

volumes:
- name: dockersock
  host:
    path: /var/run/docker.sock
```

Add a git remote that is pointing to the test repository in gogs and push the changes:

```bash
git remote add gogs http://test:test@localhost:3000/test/docker-http-server.git
git add .drone.yml
git commit -m 'Update .drone.yml'
git push gogs master
```

## Troubleshooting

Start the CI pipeline without a commit:
- restart it on the drone UI
- trigger manually the webhook in gogs

Stopping all services:

```bash
docker compose down
```

Delete all locally stored data:

```bash
sudo rm -rf ./gogs/ && sudo rm -rf ./drone/ && sudo rm -rf ./registry-data/
```

## Further hints

- slides of DevSecOps talk: https://bit.ly/2nse62q
- slides for tools: https://bit.ly/2nnOIes

## Sources

- https://github.com/katacoda/golang-http-server
