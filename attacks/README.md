# Attacks
- base images
    using latest and not sure what you get with that
    that are hijacked
        monero miner
        "phones home"
- using a base image with known security issues
    old java version
    old openssl library, heartbleed
    scan with clair
- configuration mistakes
    - registry: without authentication, without TLS ... Man-in-the-middle 
    - dockerd: publicly available docker socket ... dind
    - too much "capabilities" / rights: 
        - --privileged
        - CAP_SYSADMIN
        - --network=host
        - root user
        - mounting "/" of host
- against the CI/CD software
    - look at your Pull-Requests! it might be a pull request initiated by spaceB0x/cider!!
- using insecure hardware / firmware where spectre meltdown is not patched
